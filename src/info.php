<?php
/**
 * ----------------------------------------------------------------------------
 * Copyright 2014  Eugen Mihailescu (email : eugenmihailescux at gmail dot com)
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * ----------------------------------------------------------------------------
 *
 * Plugin Name: YAYUI Compressor (Yet Another YUI Compressor)
 * Plugin URI: http://yayui.mynixworld.info/
 * Description: YAYUI Compressor Web Interface info page
 * Author URI: http://yayui.mynixworld.info/
 *
 * Git revision information:
 *
 * @version:	0.1-2 $
 * @commit:		17c98134e99dc43433265cf28b9440b01f04828d $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Mon Oct 13 17:51:28 2014 +0200 $
 * @file:		info.php $
 *
 * @id:	info.php | Mon Oct 13 17:51:28 2014 +0200 | Eugen Mihailescu  $
 *
 */
?>
<?php

if ($standalone = (! (empty($_GET) || empty($_GET['q'])) && $_GET['q'] == 'e')) {
    include_once 'lib/YayuiCompressor.php';
    $compressor = new YayuiCompressor();
    
    echo "<!DOCTYPE html><html><head><style>";
    $style = file_get_contents('css/yayui.css');
    echo $compressor->streamCompress($style);
    echo '</style></head>';
}
?>
<table id='info_table' class='centered' style=<?php echo "'display:".($standalone?'block':'none')."'";?>>
	<tr>
		<td class='fbold'>What this appliation does:</td>
	</tr>
	<tr>
		<td><ul>
				<li>Take your source code,removes the whitespaces and shorten the
					variables names</li>
				<li>Delivers the minified/compressed source code (which
					theoretically should be fine :-)</li>
			</ul></td>
	</tr>
	<tr>
		<td class='fbold'>How it works:</td>
	</tr>
	<tr>
		<td><ul>
				<li>I use no external library/engine/parser (eg. <a
					href='http://en.wikipedia.org/wiki/Rhino_%28JavaScript_engine%29'>Rhino</a>)
					whatsoever; everything is done with the aid of plain regular
					expressions (aka <a
					href='http://en.wikipedia.org/wiki/Regular_expression'>Regex</a>)
					<ul>
						<li>there are 7 different regex's for trimming the whitespaces
							(see <i>Minify options</i>)
						</li>
						<li>there are few different regex's that together work in cascade
							helping to capture and replace the functions arguments/variables
							with shorten names, a-z (see <i>Obfuscate options</i>)
						</li>
					</ul>
				</li>
			</ul></td>
	</tr>
	<tr>
		<td class='fbold'>Caveats:</td>
	</tr>
	<tr>
		<td>
			<ul>
				<li>This application is experimental. I made it just for fun so <u>use
						it in production but with care!</u></li>
				<li>Normally a minifier/obfuscator should be done with the aid of a
					parser and not rely only on few regex patterns. Although I tested
					it against few hundred KB JavaScript source code I cannot guarantee
					that I've implemented all the possible patterns.</li>
				<li>By checking all possible options (minify/obfuscate) your code
					will still work but will not be validated by some code quality
					tools (eg. <a href='http://www.jslint.com'>JSLint</a>)
				</li>
				<li>There is still room for optimization. Perhaps some patterns can
					be writen even shorter.</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td class='fbold'>Patterns that it uses and the algorithm description:</td>
	</tr>
	<tr>
		<td><br>* for minification:
			<ol>
				<li>remove JavaScript hints : <span class='pattern'>/['"]<span
						class='keyword'>use</span>\s+<span class='keyword'>strict</span>['"]\s*;/
				</span>&nbsp;(see the <a href='#'
					onclick='popupWindow("JavaScript hint list","&lt;ol&gt;&lt;li&gt;use strict&lt;/li&gt;&lt;/ol&gt;");'>complete
						hint list</a>)
				</li>
				<li>remove dispensable semicolons : replace&nbsp;<span
					class='pattern'>/;[\s|\n]*;/m</span> &nbsp;with&nbsp;<span
					class='pattern'>;</span></li>
				<li>remove single line comments : replace&nbsp;<span class='pattern'>/(?&lt;![\'"])\/\/.*/m</span>
					&nbsp;with&nbsp;<span class='string'>'&lt;empty-string&gt;'</span>
				</li>
				<li>remove block comments : replace&nbsp;<span class='pattern'>/\/\*([^\/]|[\w\d\s]\/[\w\d\s])*\*\//m</span>
					&nbsp;with&nbsp;<span class='string'>'&lt;empty-string&gt;'</span></li>
				<li>match these blocks&nbsp;<span class='pattern'>/\b(<span
						class='keyword'>if</span>|<span class='keyword'>else</span>|<span
						class='keyword'>while</span>)\b\.*[^;\{]*(\{)([^\{\}]+)(\})/m
				</span>&nbsp;then:
					<ul>
						<li>replace the curly braces&nbsp;<span class='pattern'>/[\{\}]/</span>
							&nbsp;with&nbsp;<span class='string'>'&lt;blank&gt;'</span> if
							the code block doesn't match:
							<ul>
								<li>2+ <span class='pattern'>/;/</span> OR 1+ <span
									class='pattern'>/:/</span> OR 1+ <span class='pattern'>/\b(<span
										class='keyword'>function</span>|<span class='keyword'>try</span>|<span
										class='keyword'>catch</span>)\b/
								</span></li>
							</ul></li>
					</ul>
				</li>
				<li>remove the whitespaces&nbsp;<span class='pattern'>/\s*([^\w\d\s])\s*|[
						]{2,}/'</span> with <span class='string'>'$1'</span></li>
				<li>remove the CR/LF:
					<ul>
						<li>first replace&nbsp;<span class='pattern'>/(?&lt;=[\W])\n|[
								]{2,}/m</span> &nbsp;with&nbsp;<span class='string'>'$1'</span></li>
						<li>second replace&nbsp;<span class='pattern'>/\n/m</span> with <span
							class='string'>'&lt;blank&gt;'</span></li>
					</ul>
				</li>
			</ol></td>
	</tr>
	<tr>
		<td>* for obfuscation:
			<ol>
				<li>first backup all strings (we don't want to change them) : <span
					class='pattern'>/([\'"])(\\\\\\1|.)*?\1/m</span></li>
				<li>capture all functions: <span class='pattern'>/\b<span
						class='keyword'>function</span>\b(([\n]|.)*?)(?=[^\(]\b<span
						class='keyword'>function</span>\b\s*[\w\d]+?)/m
				</span>&nbsp;then for each:
					<ul>
						<li>capture the function declaration: <span class='pattern'>/\bfunction\b(.*?)\{/m</span>
							&nbsp;and split the arguments by comma
						</li>
						<li>for each argument (eg. <span class='string'>myVar</span>):
							<ul>
								<li>take the left part&nbsp;<span class='pattern'>/[^\w\d]/</span>
									and then replace all its occurences with your obfuscated
									variable name: replace&nbsp;<span class='pattern'>/([^\w\d\'".]*)(\b<span
										class='string'>myVar</span>\b)([^\w\d\'"]*)/
								</span>&nbsp;with <span class='string'>'$1myNewVar$3'</span></li>
							</ul></li>
						<li>capture the function inner block codes: <span class='pattern'>/\{[^\{\}]*\}/m</span>
							&nbsp;then:
							<ul>
								<li>capture the var declaration: <span class='pattern'>/\b<span
										class='keyword'>var</span>\b[^;]+/m
								</span>, split it by comma and replace&nbsp;<span
									class='pattern'>/([\s\(\[\{\+\-\*\/\|<>;&%!,])(\b<span
										class='string'>myVar</span>\b)([\s\)\]\}\+\-\*\/\|<>;&!%,.=]?)/
								</span>&nbsp;with&nbsp;<span class='string'>'$1myNewVar$3'</span></li>
							</ul></li>
					</ul></li>
				<li>restore the original strings; there is no risk in changing them
					now</li>
			</ol>
		</td>
	</tr>
	<tr>
		<td class='fbold'>FAQ</td>
	</tr>
	<tr>
		<td><ul>
				<li>Does it work for other languages, such as HTMLm CSS, PHP, Java,
					C++, etc?
					<ul>
						<li>The minification might work but the obfuscation is not
							compatible yet</li>
						<li>The HTML is not supported although this page is minified by
							itself (see the page source)</li>
					</ul>
				</li>
				<li>Can I get the source code? In what language is it written?
					<ul>
						<li>the source code has been made available at <a
							href='https://bitbucket.org/eugenmihailescu/yayui-compressor'>its
								bitbucket.org respository</a></li>
						<li>it is written in PHP (>=5.3) and encapsuled in a standalone
							class (300+ lines)
					
					</ul>
				</li>
			</ul></td>
	</tr>
</table>
<?php
if ($standalone)
    echo "</body></html>";
?>