<?php
/**
 * ----------------------------------------------------------------------------
 * Copyright 2014  Eugen Mihailescu (email : eugenmihailescux at gmail dot com)
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * ----------------------------------------------------------------------------
 *
 * Plugin Name: YAYUI Compressor (Yet Another YUI Compressor)
 * Plugin URI: http://yayui.mynixworld.info/
 * Description: Naive JavaScript minify/compress library
 * Author URI: http://yayui.mynixworld.info/
 *
 * Git revision information:
 *
 * @version:	0.1-2 $
 * @commit:		17c98134e99dc43433265cf28b9440b01f04828d $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Mon Oct 13 17:51:28 2014 +0200 $
 * @file:		YayuiCompressor.php $
 *
 * @id:	YayuiCompressor.php | Mon Oct 13 17:51:28 2014 +0200 | Eugen Mihailescu  $
 *
 */
?>
<?php

define('JSHINTS', 'jshints');
define('SEMICOLON', 'semicolon');
define('SINGLE_COMMENT', 'scomment');
define('BLOCK_COMMENT', 'bcomment');
define('CURLY_BRACE', 'curlybrace');
define('WHITESPACE', 'whitespace');
define('LINE_SEPARATOR', 'crlf');
define('FUNCTION_ARGUMENT', 'fargs');
define('FUNCTION_VARIABLE', 'fvars');
define('MINIFY', 'minify');
define('OBFUSCATE', 'obfuscate');

class YayuiCompressor
{

    private $JS_HINTS = array(
        'use strict'
    );

    private $RESERVED = array(
        'var', // "var" must have index 0!
        'break',
        'case',
        'catch',
        'continue',
        'default',
        'delete',
        'do',
        'else',
        'finally',
        'for',
        'function',
        'if',
        'in',
        'instanceof',
        'new',
        'return',
        'switch',
        'this',
        'throw',
        'try',
        'typeof',
        'void',
        'while',
        'with',
        'abstract',
        'boolean',
        'byte',
        'char',
        'class',
        'const',
        'debugger',
        'double',
        'enum',
        'export',
        'extends',
        'final',
        'float',
        'goto',
        'int',
        'interface',
        'implements',
        'import',
        'long',
        'native',
        'package',
        'private',
        'protected',
        'public',
        'short',
        'static',
        'super',
        'synchronized',
        'throws',
        'transient',
        'volatile',
        'null',
        'undefined',
        'NaN',
        'true',
        'false'
    );

    private $SOURCE_SEP = array(
        '{',
        '}'
    );

    private static $VALID_OPTIONS = array(
        JSHINTS,
        SEMICOLON,
        SINGLE_COMMENT,
        BLOCK_COMMENT,
        CURLY_BRACE,
        WHITESPACE,
        LINE_SEPARATOR,
        FUNCTION_ARGUMENT,
        FUNCTION_VARIABLE
    );

    private $OBFUSC_SEP;

    private $_ratio;

    private $_statistic;

    function __construct()
    {
        $this->ratio = 0;
        $this->OBFUSC_SEP = array(
            uniqid('_BEGIN_', true),
            uniqid('_END_', true)
        );
    }

    static public function getFactoryDefaults()
    {
        return array(
            JSHINTS => true,
            SEMICOLON => true,
            SINGLE_COMMENT => true,
            BLOCK_COMMENT => true,
            CURLY_BRACE => true,
            WHITESPACE => true,
            LINE_SEPARATOR => true,
            FUNCTION_ARGUMENT => true,
            FUNCTION_VARIABLE => true
        );
    }

    static public function getOptionSet($set_id = -1)
    {
        $obfuscate_options = array(
            FUNCTION_ARGUMENT => true,
            FUNCTION_VARIABLE => true
        );
        
        switch ($set_id) {
            case MINIFY:
                $result = array_diff_assoc(self::getFactoryDefaults(), $obfuscate_options);
                break;
            case OBFUSCATE:
                $result = $obfuscate_options;
                break;
            default:
                $result = self::getFactoryDefaults();
                break;
        }
        return $result;
    }

    static public function getParamValue($options, $param_name)
    {
        if (! empty($options) && array_key_exists($param_name, $options) && is_bool($options[$param_name]))
            return $options[$param_name];
        return in_array($param_name, $options, true);
    }

    /**
     * Generates a variable name base on $nr
     *
     * @param int $nr            
     * @param int $base            
     * @return string
     */
    private function _getVarName($nr, $base = 26)
    {
        $digits = 1 + floor(($nr > 0 ? log($nr, $base) : 0));
        $var_name = '';
        for ($i = $digits - 1; $i >= 0; $i --) {
            $p = pow($base, $i);
            $q = floor($nr / $p);
            $var_name .= chr(65 + $q - ($i == 0 ? 0 : 1));
            $nr -= $q * $p;
        }
        
        return $var_name;
    }

    private function _initStatistic()
    {
        foreach (self::$VALID_OPTIONS as $option)
            $this->_statistic[$option] = array(
                'size' => 0,
                'time' => 0
            );
    }

    private function _trim($str)
    {
        $start = microtime();
        $buffer_len = strlen($str);
        $buffer = trim($str);
        $this->_updateStatistic(WHITESPACE, $buffer_len - strlen($buffer), microtime() - $start);
        return $buffer;
    }

    private function _updateStatistic($option_name = null, $count = 0, $duration = 0)
    {
        if (! empty($option_name)) {
            $this->_statistic[$option_name]['size'] += $count;
            $this->_statistic[$option_name]['time'] += ($duration > 0 ? $duration : 0);
        }
    }

    private function _preg_replace1($pattern, $replacement, $subject, $option_name = null)
    {
        return $this->_preg_replace($pattern, $replacement, $subject, - 1, $rc, $option_name);
    }

    private function _preg_replace($pattern, $replacement, $subject, $limit = -1, &$count = null, $option_name = null)
    {
        $start = microtime();
        if (null == $count)
            $count = 0;
        $buffer = call_user_func('preg_replace' . (is_callable($replacement) ? '_callback' : ''), $pattern, $replacement, $subject, $limit, $count);
        $this->_updateStatistic($option_name, strlen($subject) - strlen($buffer), microtime() - $start);
        
        return $buffer;
    }

    /**
     * Minimizes the $buffer source code by trimming all possible whitespaces
     *
     * @param string $buffer            
     * @return string
     */
    private function _sourceMinify($buffer, $options)
    {
        
        // remove unnecessary double-semicolon
        if (self::getParamValue($options, SEMICOLON)) {
            $buffer = $this->_preg_replace1('/;[\s|\n]*;/m', ';', $buffer, SEMICOLON);
        }
        // remove unnecessary semicolon before curly brace;
        // WARNING: it may break the code because doesn't know to handle anonymous functions
        // $buffer = preg_replace('/\}\s*;/m', '', $buffer);
        
        // remove single element comments (eg. "//")
        if (self::getParamValue($options, SINGLE_COMMENT)) {
            $buffer = $this->_preg_replace1('/(?<![\'"])\/\/.*/m', '', $buffer, SINGLE_COMMENT);
        }
        // remove block comments (eg. /* ... */)
        if (self::getParamValue($options, BLOCK_COMMENT)) {
            $buffer = $this->_preg_replace1('/\/\*([^\/]|[\w\d\s]\/[\w\d\s])*\*\//m', '', $buffer, BLOCK_COMMENT);
        }
        
        if (self::getParamValue($options, CURLY_BRACE)) {
            $callback = function ($match)
            {
                if (preg_match_all('/;/', $match[0], $matches) > 1 || preg_match_all('/:/', $match[0], $matches) > 0 || preg_match_all('/\b(function|try|catch)\b/', $match[0], $matches) > 0)
                    return $match[0];
                else
                    return $this->_preg_replace1('/[\{\}]/', ' ', $match[0], CURLY_BRACE);
            };
            // remove unnecessary curly braces
            $old_buffer = null;
            while ($old_buffer != $buffer) {
                $old_buffer = $buffer;
                $buffer = $this->_preg_replace1('/\b(if|else|while)\b\.*[^;\{]*(\{)([^\{\}]+)(\})/m', $callback, $buffer, CURLY_BRACE);
            }
        }
        // remove spaces between operands and operators
        if (self::getParamValue($options, WHITESPACE)) {
            $buffer = $this->_preg_replace1('/\s*([^\w\d\s])\s*|[ ]{2,}/', '$1', $buffer, WHITESPACE);
        }
        // remove CR/LF
        if (self::getParamValue($options, LINE_SEPARATOR)) {
            $buffer = $this->_preg_replace1('/(?<=[\W])\n|[ ]{2,}/m', '$1', $buffer, LINE_SEPARATOR);
            $buffer = $this->_preg_replace1('/\n/m', ' ', $buffer, LINE_SEPARATOR);
        }
        return $this->_trim($buffer);
    }

    /**
     * Obfuscate the innerst level of the source code
     *
     * @param array $data            
     * @param int $index            
     * @return array
     */
    private function _obfuscateCodeBlockVars($data, $index = 0)
    {
        // split the code into code blocks; we'll get only the 'leaf' blocks
        // a 'leaf block' doesn't contain '{...}'
        if (($found = preg_match_all('/\{[^\{\}]*\}/m', $data, $blocks)) > 0)
            foreach ($blocks[0] as $src_block) {
                // replace the { and } with some temporary symbols eg. '@#$&' (such that we can easily ignore this block next time)
                $tmp_block = str_replace($this->SOURCE_SEP, $this->OBFUSC_SEP, $src_block);
                
                // recursively obfuscate the $data untile there is no '{...}' left
                $obfus_block = $this->_obfuscateCodeBlockVars($tmp_block, $index);
                
                $index = $obfus_block['index'];
                
                // replace the original block with the obfuscated one ({...} => '@#$&' ... '@#$&')
                $block = substr($obfus_block['data'], 1, strlen($obfus_block['data']) - 2);
                
                // echo str_repeat('-', 80) . PHP_EOL;
                // echo $block . PHP_EOL;
                // echo str_repeat('-', 80) . PHP_EOL;
                
                // extract var declaration lines from the current code block
                if (preg_match_all('/\b' . $this->RESERVED[0] . '\b[^;]+/m', $block, $var_lines) > 0) {
                    foreach ($var_lines[0] as $line) {
                        $short_line = substr($line, strpos($line, ' '));
                        
                        // make sure that we strip temporarly the block comments such that we extract only the arguments names and nothing else
                        $short_line = preg_replace('/\/\*([^\/]|[\w\d\s]\/[\w\d\s])*\*\//m', '', $short_line);
                        
                        // extract the comma-delimited variables
                        foreach (preg_split('/,/', $short_line) as $var_def) {
                            
                            // extract only the token on the left of '=' (if any)
                            
                            $match = preg_split('/=/', $var_def);
                            $v = trim($match[0]);
                            
                            // obfuscate ONLY the valid var's name
                            if (! (0 !== preg_match('/[^\w\d]/', $v) || in_array($v, $this->RESERVED) || is_numeric($v) || 0 === strlen($v))) {
                                $obsf_name = strtolower($this->_getVarName($index ++));
                                // pattern anterior /([^\w\d\'".]+)(\b' . $v . '\b)([^\w\d\'"]*)/
                                if ($v != $obsf_name)
                                    do
                                        // '/([\s\(\[\{\+\-\*\/\|<>;&%!,])(\b'.$v.'\b)([\s\)\]\}\+\-\*\/\|<>;&!%,.=]?)/'
                                        // old pattern: '/([^\w\d\'".]*)(\b' . $v . '\b)([^\w\d\'"]*)/'
                                        // '([^\w\d\'".])(\bobj\b)([^\w\d\'"]?)'
                                        $obfus_block['data'] = $this->_preg_replace('/([\s\(\[\{\+\-\*\/\|<>;&%!,])(\b' . $v . '\b)([\s\)\]\}\+\-\*\/\|<>;&!%,.=]?)/', '$1' . $obsf_name . '$3', $obfus_block['data'], - 1, $rc, FUNCTION_VARIABLE); while ($rc > 0);
                                else
                                    $index --;
                            }
                        }
                    }
                }
                
                // replace the original code block with the obfuscated data
                $data = str_replace($src_block, $obfus_block['data'], $data);
            }
        return array(
            'found' => $found > 0,
            'data' => $data,
            'index' => $index
        );
    }

    /**
     * Obfuscate the local variable names of a inner code block (eg.
     * if{_inner_code}else{_inner_code}
     *
     * @param string $buffer            
     * @return string
     */
    private function _obfuscateCodeBlock($buffer, $index = 0)
    {
        $obf_data = array(
            'found' => true,
            'data' => $buffer,
            'index' => $index
        );
        
        while ($obf_data['found'])
            $obf_data = $this->_obfuscateCodeBlockVars($obf_data['data'], $obf_data['index']);
        
        return str_replace($this->OBFUSC_SEP, $this->SOURCE_SEP, $obf_data['data']);
    }

    /**
     * Obfuscate the function arguments then continues obfuscating the function inner variable names
     *
     * @param string $func_block
     *            The function code block whose variable names has to be obfuscated
     * @return string Returns the obfuscated string buffer
     */
    private function _obfuscateFunctionArgs($func_block, $options)
    {
        $index = 0;
        
        if (self::getParamValue($options, FUNCTION_ARGUMENT) && preg_match_all('/\bfunction\b(.*?)\{/m', $func_block, $params) > 0) {
            foreach ($params[0] as $vars) {
                $p1 = strpos($vars, '(');
                $p2 = strrpos($vars, ')');
                $items = substr($vars, $p1 + 1, $p2 - $p1 - 1);
                
                // make sure that we strip temporarly the block comments such that we extract only the arguments names and nothing else
                $items = preg_replace('/\/\*([^\/]|[\w\d\s]\/[\w\d\s])*\*\//m', '', $items);
                // echo 'dupa excludere block comment codul arata astfel:' . PHP_EOL . $items . PHP_EOL;
                foreach (preg_split('/,/', $items) as $param) {
                    $match = preg_split('/[^\w\d]/', trim($param));
                    $v = trim($match[0]);
                    
                    if (! (0 !== preg_match('/[^\w\d]/', $v) || in_array($v, $this->RESERVED) || is_numeric($v) || 0 === strlen($v))) {
                        
                        $obsf_name = strtolower($this->_getVarName($index ++));
                        
                        if ($v != $obsf_name)
                            do
                                $func_block = $this->_preg_replace('/([^\w\d\'".]*)(\b' . $v . '\b)([^\w\d\'"]*)/', '$1' . $obsf_name . '$3', $func_block, - 1, $rc, FUNCTION_ARGUMENT); while ($rc > 0);
                        else
                            $index --;
                    }
                }
            }
        }
        
        // echo str_repeat('#', 80) . PHP_EOL;
        // echo 'dupa ce am replaciuit argumentele functiei blocul arata asa:' . PHP_EOL;
        // echo $func_block . PHP_EOL;
        // echo str_repeat('#', 80) . PHP_EOL;
        if (self::getParamValue($options, FUNCTION_VARIABLE))
            return $this->_obfuscateCodeBlock($func_block, $index);
        else
            return $func_block;
    }

    /**
     * Parse all the functions and obfuscate functions' variable name.
     *
     * @param string $data
     *            The input string buffer
     * @return string Returns the compressed buffer
     */
    private function _obfuscateAllFunctions($data, $options)
    {
        $dummy_function = "\nfunction dummy(){}"; // we need a dummy function due to the pattern bellow is half-dumb!
        $data .= $dummy_function;
        
        // split the code into function blocks; we'll get only the 'leaf' blocks
        // a 'leaf block' doesn't contain 'function(){...}'
        // Note that this will not process the last function
        if (($f1 = preg_match_all('/\bfunction\b(([\n]|.)*?)(?=[^\(]\bfunction\b\s*[\w\d]+?)/m', $data, $blocks)) > 0) {
            foreach ($blocks[0] as $block) {
                $func_block = substr($block, 0, strrpos($block, '}') + 1); // extract the function block
                $src_block = $func_block;
                
                $func_block = $this->_obfuscateFunctionArgs($func_block, $options);
                $data = str_replace($src_block, $func_block, $data);
            }
        }
        
        return str_replace($dummy_function, '', $data);
    }

    static public function getValidOptionNames()
    {
        return self::$VALID_OPTIONS;
    }

    static public function getOptionDescription($name)
    {
        switch ($name) {
            case JSHINTS:
                $result = 'removes the JavaScript hints (eg. "use strict" assertion)';
                break;
            case SEMICOLON:
                $result = 'removes the unnecessary semicolons';
                break;
            case SINGLE_COMMENT:
                $result = 'removes the single comment lines (eg. //...)';
                break;
            case BLOCK_COMMENT:
                $result = 'removes the block comment lines (eg. /*...*/)';
                break;
            case CURLY_BRACE:
                $result = 'removes the unnecessary curly braces (if|else|while)';
                break;
            case WHITESPACE:
                $result = 'removes the whitespaces as much as possible';
                break;
            case LINE_SEPARATOR:
                $result = 'removes the lines separators (cr/lf)';
                break;
            case FUNCTION_ARGUMENT:
                $result = 'obfuscates the function arguments names';
                break;
            case FUNCTION_VARIABLE:
                $result = 'obfuscates the function code variables names';
                break;
            default:
                $result = '';
                break;
        }
        return $result;
    }

    /**
     * Returns the last compression ratio
     *
     * @return number
     */
    public function getRatio()
    {
        return $this->_ratio;
    }

    public function getStatistics()
    {
        return $this->_statistic;
    }

    /**
     * Minify/obfuscate a JavaScript file
     *
     * @param string $in_file
     *            The filename to be minified/obfuscate
     * @param string $out_file
     *            Where to write the compressed result
     * @param bool $minify
     *            true if you want to strip whitespaces
     * @param bool $obfuscate
     *            true if you want also to obfuscate the code (otherwise it's only minified)
     */
    public function compress($in_file, $out_file, $options = null)
    {
        if (! file_exists($in_file))
            throw new Exception(sprintf('Input file "%s" not found.', $in_file));
        
        $buffer = file_get_contents($in_file);
        $in_size = filesize($in_file);
        
        $buffer = $this->streamCompress($buffer, $options);
        
        file_put_contents($out_file, $buffer);
        $out_size = strlen($buffer);
        
        $this->_ratio = 100 * (1 - $out_size / $in_size);
    }

    /**
     * Minify/obfuscate a JavaScript buffer string
     *
     * @param string $buffer
     *            The input string containing the JavaScript code
     * @param bool $minify
     *            true if you want to strip whitespaces
     * @param bool $obfuscate
     *            true if you want also to obfuscate the code (otherwise it's only minified)
     * @return string The minified/obfuscated JavaScript string buffer
     */
    public function streamCompress($buffer, $options = null)
    {
        if (empty($options))
            $options = self::getFactoryDefaults();
        elseif (! (is_array($options) /*&& count(array_intersect(array_keys($options), self::$VALID_OPTIONS)) !== 0*/)) {
            throw new Exception("Invalid options' format.");
        }
        // init compression statistics
        $this->_initStatistic();
        
        $in_size = strlen($buffer);
        
        $minify = (self::getParamValue($options, JSHINTS) || self::getParamValue($options, SEMICOLON) || self::getParamValue($options, SINGLE_COMMENT) || self::getParamValue($options, BLOCK_COMMENT) || self::getParamValue($options, CURLY_BRACE) || self::getParamValue($options, WHITESPACE));
        
        $obfuscate = (self::getParamValue($options, FUNCTION_VARIABLE) || self::getParamValue($options, FUNCTION_ARGUMENT));
        
        if ($minify || $obfuscate) {
            $removed_strings = array();
            $callback = function ($match) use(&$removed_strings) // PHP >= 5.3 only
            {
                $id = uniqid('mask_', true);
                $removed_strings[$id] = $match[0];
                return $id;
            };
            
            // remove unnecessary hints (eg. "use strict" assertion); MUST be done before string backup
            if (self::getParamValue($options, JSHINTS)) {
                $hint_str = implode('|', $this->JS_HINTS);
                $buffer = $this->_preg_replace1('/[\'"]' . str_replace(' ', '\s+', $hint_str) . '[\'"]\s*;/', '', $buffer, JSHINTS);
            }
            
            // backup all strings so we don't have to worry about replacing their content by accident
            $buffer = preg_replace_callback('/([\'"])(\\\\\\1|.)*?\1/m', $callback, $buffer) . PHP_EOL;
            
            // MUST be done before minification
            if ($obfuscate) {
                $buffer = $this->_obfuscateAllFunctions($buffer, $options);
            }
            
            if ($minify) {
                $buffer = $this->_sourceMinify($buffer, $options);
            }
            
            // finally restore the removed strings
            $buffer = str_replace(array_keys($removed_strings), $removed_strings, $buffer);
        }
        
        $out_size = strlen($buffer);
        $this->_ratio = 100 * (1 - $out_size / $in_size);
        
        return $buffer;
    }
}
