<?php
/**
 * ----------------------------------------------------------------------------
 * Copyright 2014  Eugen Mihailescu (email : eugenmihailescux at gmail dot com)
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * ----------------------------------------------------------------------------
 *
 * Plugin Name: YAYUI Compressor (Yet Another YUI Compressor)
 * Plugin URI: http://yayui.mynixworld.info/
 * Description: Web Interface for YAYUI Compressor
 * Author URI: http://yayui.mynixworld.info/
 *
 * Git revision information:
 *
 * @version:	0.1-2 $
 * @commit:		17c98134e99dc43433265cf28b9440b01f04828d $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Mon Oct 13 17:51:28 2014 +0200 $
 * @file:		index.php $
 *
 * @id:	index.php | Mon Oct 13 17:51:28 2014 +0200 | Eugen Mihailescu  $
 *
 */
?>
 
 <?php

define("YAYUI_VERSION", '@version:	0.1-2 $');
define("YAYUI_VERSION_DATE", '@short_date:	2014-10-13 $');
define("YAYUI_AUTHOR", '@author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $');

include_once 'lib/YayuiCompressor.php';

function minify($buffer)
{
    $in = strlen($buffer);
    $removed_strings = array();
    $callback = function ($match) use(&$removed_strings) // PHP >= 5.3 only
    {
        $id = uniqid('mask_', true);
        $removed_strings[$id] = $match[0];
        return $id;
    };
    $buffer = preg_replace_callback('/([\'"])(\\\\\\1|.)*?\1/m', $callback, $buffer) . PHP_EOL; // string backup
    $buffer = preg_replace('/[\r\n]+/', '', $buffer);
    $buffer = preg_replace('/[\s\t]+/', ' ', $buffer);
    $buffer = preg_replace('/>([\s\t\n\r]+)</', '><', $buffer);
    $patterns = array(
        '/<!\-{2}[^>]*\-{2}>/m',
        '/(<)(\s+)([\w\/>]+)/m',
        '/([\w\d\/]+)(\s+)(>)/m',
        '/([\w\d]+)(\s+)([^\w\d]+)/m',
        '/([^\w\d]+)(\s+)([\w\d]+)/m'
    );
    $buffer = preg_replace($patterns, '$1$3', $buffer);
    $buffer = str_replace(array_keys($removed_strings), $removed_strings, $buffer);
    $out = strlen($buffer);
    $buffer = sprintf('<!-- HTML minified by YAYUI ~%5.2f%% (%d bytes)-->%s%s', 100 * (1 - $in / $out), $in - $out, PHP_EOL, $buffer);
    return $buffer;
}

function formatError($message, $color = null, $status = false)
{
    return '<span class="caption">' . ($status ? 'Status : ' : '') . (empty($color) ? '' : '</span><span style="color:' . $color . '">') . $message . '</span>';
}

function validateSupportForm($params)
{
    $error = null;
    if (! is_array($params))
        die(formatError('Invalid message content', '#f00'));
    elseif (empty($params['email']) || ! preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $params['email']))
        $error = 'e-mail address';
    elseif (empty($params['sender']) || strlen($params['sender']) < 3)
        $error = 'contact name';
    elseif (empty($params['body']) || strlen($params['body']) < 30 || preg_match_all('/\s/', $params['body'], $matches) < 4)
        $error = 'message';
    
    if (! empty($error)) {
        die(formatError('Please give me a more descriptive <b>' . $error . '</b>.<br>The more info details, the better!', 'red'));
    } else
        return true;
}

function processSupportForm(&$method)
{
    if (true !== validateSupportForm($method))
        return;
    
    $subject = 'Feedback ' . $_SERVER['SERVER_NAME'];
    if (! empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (! empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    
    $message = sprintf('New feedback received from %s, e-mail <%s>(%s)\r\n%s', $method['sender'], $method['email'], $ip, $method['body']);
    
    // send the email; note that mail function requires proper configuration in php.ini
    $mail_sent = @mail('eugenmihailescux@gmail.com', $subject, $message);
    
    $likely_err = print_r(error_get_last(), true);
    // if the message is sent successfully print "Mail sent". Otherwise print "Mail failed"
    $send_status = $mail_sent ? "Your message was sent successfuly" : empty($likely_err) ? "Your message could not be sent.<br>Mail failed (I don't know exactly why)" : $likely_err;
    
    if (isset($send_status)) {
        echo formatError($send_status, ($mail_sent ? 'green' : 'red'));
    }
}

function getVersionInfo()
{
    if (1 === preg_match('/[\d.-]+/', YAYUI_VERSION, $matches))
        $result['version'] = $matches[0];
    if (1 === preg_match('/[\d.-]+/', YAYUI_VERSION_DATE, $matches))
        $result['version_date'] = $matches[0];
    if (1 === preg_match('/@author:\s*([^$]*)/', YAYUI_AUTHOR, $matches)) {
        $author = $matches[1];
        if (1 === preg_match('/^\b[\w\s]+\b/', $author, $matches))
            $result['author_name'] = $matches[0];
        if (1 === preg_match('/<(.*)>/', $author, $matches))
            $result['author_email'] = $matches[1];
    }
    
    return $result;
}

if (! (empty($_POST) || empty($_POST['send_feedback'])))
    return processSupportForm($_POST);
    
    // filter the output through minify filter
ob_start();

define('COPYRIGHT_FROM', 2014);
$copyright_to = date('Y', time());
if ($copyright_to != COPYRIGHT_FROM)
    $copyright = sprintf('%d - %d', COPYRIGHT_FROM, $copyright_to);
else
    $copyright = COPYRIGHT_FROM;

$version_info = getVersionInfo();

$author = $version_info['author_name'];
$author_email = $version_info['author_email'];
$version = $version_info['version'];
$version_date = $version_info['version_date'];
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" id='font-awesome'
	href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<link rel='stylesheet' id='yayui_css' href='css/yayui.css'
	type='text/css' media='all' />
<script src="js/yayui.js" id='yayui'></script>
<script type="text/javascript" id='google_analytics'>var _gaq = _gaq || [];_gaq.push(['_setAccount', 'UA-47078104-3']);_gaq.push(['_gat._forceSSL']);_gaq.push(['_trackPageview']);</script>
<title>Yet Another YUI Compressor - testing room</title>
</head>
<body>
	<h1 style='text-align: center'>YAYUI, Yet Another YUI compressor</h1>
	<p style='text-align: center'><?php echo sprintf('version %s (%s)',$version,$version_date);?></p>
	<div class='feedback-box' id='feedback_box' style='border-radius: 0px;'>
		<span style='padding: 5px; font-size: 1.5em; font-weight: bold;'>Feedback
			form</span>
		<div class='feedback-btn' id='feedback_btn'
			onclick='toggle_feedbackbox();'>
			<i class="fa fa-bullhorn fa-flip-horizontal"></i>&nbsp;Feedback

		</div>
		<table style='width: 100%; padding: 10px;'>
			<tr>
				<td>Your name:</td>
				<td><input id='sender' type="text" style='width: 100%'></td>
				<td style='padding: 5px; color: #f00; text-align: center;'>*</td>
			</tr>
			<tr>
				<td>Email address:</td>
				<td><input id='email' type="email" style='width: 100%'></td>
				<td style='padding: 5px; color: #f00; text-align: center;'>*</td>
			</tr>
			<tr>
				<td colspan="3">Your message:</td>
			</tr>
			<tr>
				<td colspan="3"><textarea id='body'
						style='width: 100%; height: 10em; resize: none;'></textarea></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3" style='text-align: center'><div class='button'
						style='display: inline; margin-left: auto; margin-right: auto'
						onclick='sendFeedback();'>Send your message</div></td>
			</tr>
		</table>
	</div>

	<div id='main' class='main'>
		<table class='centered'>
			<tr>
				<td><span id='info_btn' class='button-primary'
					style='text-align: center'
					onclick="_gaq.push(['_trackEvent', 'yayui-info', 'clicked']);info();">More
						info &gt;&gt;</span></td>
			</tr>
		</table>
		<table id='main_table' class='centered'>
			<tr>
				<td class='fbold'>Your JavaScript source code</td>
			</tr>
			<tr>
				<td><textarea style='max-width: 730px; min-width: 730px;' rows="20"
						cols="100" id='source_code'></textarea></td>
			</tr>
			<tr>
				<td><table style='width: 100%'>
						<tr>
							<td class='header'><input type="checkbox" id='minify'
								value='minify' onclick='toggle_children(this);' checked>Minify
								options</td>
							<td class='header'><input type="checkbox" id='obfuscate'
								value='obfuscate' onclick='toggle_children(this);' checked>Obfuscate
								options</td>
						</tr>
						<tr>
							<td><input type="checkbox" id='jshints' name='minify_jshints'
								value='jshints' onclick='toggle_parent(this);' checked>remove
								JavaScripts hints (eg. "use strict")</td>
							<td><input type="checkbox" id='fargs' value='fargs'
								name='obfuscate_fargs' onclick='toggle_parent(this);' checked>obfuscate
								function arguments</td>
						</tr>
						<tr>
							<td><input type="checkbox" id='semicolon' name='minify_semicolon'
								onclick='toggle_parent(this);' checked>remove dispensable
								semicolons (eg. ; ;)</td>
							<td><input type="checkbox" id='fvars' value='fvars'
								name='obfuscate_fvars' onclick='toggle_parent(this);' checked>obfuscate
								function arguments</td>
						</tr>
						<tr>
							<td><input type="checkbox" id='scomment' name='minify_scomment'
								value='scomment' onclick='toggle_parent(this);' checked>removes
								the single comment lines (eg. //...)</td>
							<td><input type="checkbox" id='fnames' name='obfuscate_fnames'
								value='fnames' onclick='toggle_parent(this);' disabled><span
								style='color: rgba(0, 0, 0, 0.5);'>obfuscate function names</span></td>
						</tr>
						<tr>
							<td colspan="2"><input type="checkbox" id='bcomment'
								name='minify_bcomment' value='bcomment'
								onclick='toggle_parent(this);' checked>removes the block comment
								lines (eg. /*...*/)</td>
						</tr>
						<tr>
							<td colspan="2"><input type="checkbox" id='curlybrace'
								name='minify_curlybrace' value='curlybrace'
								onclick='toggle_parent(this);' checked>removes dispensable curly
								braces (around if|else|while)</td>
						</tr>
						<tr>
							<td colspan="2"><input type="checkbox" id='whitespace'
								name='minify_whitespace' value='whitespace'
								onclick='toggle_parent(this);' checked>removes the whitespaces
								as much as possible (eg. " a != b ;" =&gt; "a!=b;")</td>
						</tr>
						<tr>
							<td colspan="2"><input type="checkbox" id='crlf' value='crlf'
								name='minify_crlf' onclick='toggle_parent(this);' checked>removes
								the lines separators (CR/LF)</td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td><table id='output_table' class='centered' style='display: none'>
						<tr>
							<td class='fbold'>Your JavaScript compressed source code</td>
						</tr>
						<tr>
							<td><textarea style='max-width: 720px; min-width: 720px;'
									rows="20" cols="100" id='output'></textarea></td>
						</tr>
					</table></td>
			</tr>
		</table>
		<table class='centered'>
			<tr>
				<td><span id='exec_btn' class='button' style='text-align: center'
					onclick="_gaq.push(['_trackEvent', 'yayui-compress', 'clicked']);compress();">Compress
						source</span></td>
			</tr>
		</table>
		
<?php include_once 'info.php';?>

	</div>
	<div id='footer' class='footer'>
		<p>
			<abbr title=<?php echo "'Copyright $copyright'";?>>&copy;</abbr>
			<?php echo $author?>
		</p>
		<p>
			<a href="#" onclick='info();'>Terms</a> | <a
				href=<?php echo "'mailto:$author_email'"?>>Contact</a>
		</p>
	</div>
	<?php $uid=uniqid('_YAYUI_JS_'); echo $uid;?>
</body>
</html>

<?php

$html = ob_get_contents();
$html = minify($html);

ob_end_clean();

$jsq[] = "(function() {
    var ga = document.createElement('script');     ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:'   == document.location.protocol ? 'https://ssl'   : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
	 _gaq.push(['_trackEvent', 'yayui', 'visit']);";
$jsq[] = "function sendFeedback(){
    var formData = new FormData(), fields=['sender','email','body'],i;
    formData.append('send_feedback','1');
     for(i=0;i<fields.length;i+=1){
        formData.append(fields[i],document.getElementById(fields[i]).value);
     }
     onready=function(xmlhttp){popupWindow('Sent status',xmlhttp.responseText)};
    asyncRunJob(location.href,formData,'E-mail',null,null,4,'@#$%',null,onready);}";

$buffer = "<script type='text/javascript' id='feedback'>" . implode('', $jsq) . "</script>";
$in = strlen($buffer);
$yayui = new YayuiCompressor();
$buffer = $yayui->streamCompress($buffer);
$out = strlen($buffer);

$buffer = sprintf(PHP_EOL . '<!-- JavaScript minified by YAYUI ~%5.2f%% (%d bytes)-->%s%s', 100 * (1 - $in / $out), $in - $out, PHP_EOL, $buffer);

echo str_replace($uid, $buffer, $html);

?>