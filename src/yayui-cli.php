<?php
/**
 * ----------------------------------------------------------------------------
 * Copyright 2014  Eugen Mihailescu (email : eugenmihailescux at gmail dot com)
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2, as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * ----------------------------------------------------------------------------
 *
 * Plugin Name: YAYUI Compressor (Yet Another YUI Compressor)
 * Plugin URI: http://yayui.mynixworld.info/
 * Description: YAYUI Compressor CLI interface
 * Author URI: http://yayui.mynixworld.info/
 *
 * Git revision information:
 *
 * @version:	0.1-2 $
 * @commit:		cc4f268a6af72a2572e15551d44a1d1aadbb6aa7 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sun Oct 12 22:10:06 2014 +0200 $
 * @file:		yayui-cli.php $
 *
 * @id:	yayui-cli.php | Sun Oct 12 22:10:06 2014 +0200 | Eugen Mihailescu  $
 *
 */
?>
<?php

include_once 'lib/YayuiCompressor.php';

define("YAYUI_VERSION", '@version:	0.1-2 $');
define("YAYUI_VERSION_DATE", '@short_date:	2014-10-13 $');
define("YAYUI_AUTHOR", '@author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $');

function sendFile($name)
{
    if (file_exists($name))
        $fsize = filesize($name);
    else
        die('File "' . $name . '" does not exists and thus cannot be downloaded (is it hidden?)');
    
    try {
        $chunksize = 1048576; // 1Mb buffer
        if ($fsize > $chunksize) {
            $handle = fopen($name, 'rb');
            while (! feof($handle)) {
                echo fread($handle, $chunksize);
                flush();
                @ob_end_flush();
            }
            fclose($handle);
        } else {
            readfile($name);
            flush();
            @ob_end_flush();
        }
        unlink($name);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function parseArguments($argv, $x)
{
    $options = array();
    if (YayuiCompressor::getParamValue($argv, '-minify'))
        $options = YayuiCompressor::getOptionSet(MINIFY);
    elseif (YayuiCompressor::getParamValue($argv, '-obfuscate')) {
        $options = YayuiCompressor::getOptionSet(OBFUSCATE);
    } else {
        if ($x) {
            $argv = array_keys($argv);
            $index = 0;
        } else
            $index = 3;
        
        for ($i = $index; $i < count($argv); $i ++)
            foreach (YayuiCompressor::getValidOptionNames() as $valid_option) {
                if (! empty($argv[$i])) {
                    if ($argv[$i] == '-' . $valid_option)
                        $options[$valid_option] = true;
                    else
                        $options[str_replace('-', '', $argv[$i])] = true;
                }
            }
    }
    return $options;
}

function echoHelp($error = null)
{
    global $argv;
    if (! empty($error))
        echoStatus($error);
    $is_cli = true;
    if (array_key_exists(0, $argv))
        $app_id = basename($argv[0]);
    elseif (! empty($_SERVER) && array_key_exists('SCRIPT_NAME', $_SERVER)) {
        $app_id = $_SERVER['SCRIPT_NAME'];
    } else
        throw Exception('Invalid application call.<br>What application are you using anyway?');
    
    $version_info = getVersionInfo();
    $msg = sprintf('YAYUI Compressor %s, %s%s(aka Yet Another YUI Compressor) ', $version_info['version'], $version_info['version_date'], PHP_EOL) . PHP_EOL . PHP_EOL;
    $msg .= 'Syntax:' . PHP_EOL . "\t" . $app_id . ' input-file output-file [options]' . PHP_EOL;
    $msg .= 'where' . PHP_EOL;
    $msg .= "- input-file: your uncompressed JavaScript source file" . PHP_EOL;
    $msg .= "- output-file: the destination where to save the compressed/minified file" . PHP_EOL . PHP_EOL;
    $msg .= 'Options:' . PHP_EOL;
    foreach (YayuiCompressor::getValidOptionNames() as $valid_option)
        $msg .= sprintf("\t-%-10s: %s", $valid_option, YayuiCompressor::getOptionDescription($valid_option)) . PHP_EOL;
    $msg .= sprintf("\t-%-10s: %s", 'minify', 'it\'s a shortcut; encloses options related to minification (all EXCEPT fargs and fvars)') . PHP_EOL;
    $msg .= sprintf("\t-%-10s: %s", 'obfuscate', 'it\'s a shortcut; encloses options related to obfuscation (ONLY fargs and fvars)') . PHP_EOL;
    $msg .= sprintf("\t-%-10s: %s", 'statistic', 'on success displays detailed statistics (type,duration,count)') . PHP_EOL;
    $msg .= sprintf("\t-%-10s: %s", 'help, -h', 'prints this help message') . PHP_EOL;
    $msg .= 'When no option is specified at all then all options are regarded as being set.' . PHP_EOL;
    $msg .= PHP_EOL . 'Copyright ' . date('Y', time()) . ' ' . $version_info['author_name'] . PHP_EOL;
    $msg .= wordwrap('This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License, version 2, as published by the Free Software Foundation.') . PHP_EOL;
    $msg .= PHP_EOL . 'The source code may be found at: https://bitbucket.org/eugenmihailescu/yayui-compressor' . PHP_EOL;
    $msg .= PHP_EOL . 'Send your feedback, suggestion at ' . $version_info['author_email'] . PHP_EOL;
    
    die($msg);
}

function echoStatus($status)
{
    echo str_repeat('#', strlen($status) + 4) . PHP_EOL;
    echo sprintf('* %s *' . PHP_EOL, $status);
    echo str_repeat('#', strlen($status) + 4) . PHP_EOL . PHP_EOL;
}

function echoStatistic($stat_array, $total)
{
    // echoStatus('Statistic details');
    echo str_repeat('-', 49) . PHP_EOL;
    echo sprintf('| %-15s%10s%10s%10s |', 'Operation', 'Duration', 'Count', 'Ratio') . PHP_EOL;
    echo sprintf('| %-15s%10s%10s%10s |', 'type', '(seconds)', '(bytes)', '(%)') . PHP_EOL;
    echo str_repeat('-', 49) . PHP_EOL;
    $total_0 = 0;
    $total_1 = 0;
    foreach ($stat_array as $key => $item) {
        $total_0 += $item['size'];
        $total_1 += $item['time'];
        $ratio = 100 * $item['size'] / $total;
        $hint = $ratio > 50 ? ' <- WoW!' : '';
        echo sprintf('| %-15s%10.3f%10d%10.2f |%s' . PHP_EOL, $key, $item['time'] / 1000, $item['size'], $ratio, $hint);
    }
    
    echo str_repeat('-', 49) . PHP_EOL;
    echo sprintf('| %-15s%10.3f%10d%10.2f |' . PHP_EOL, 'TOTAL', $total_1 / 1000, $total_0, 100 * $total_0 / $total);
    echo str_repeat('-', 49) . PHP_EOL;
}

function getVersionInfo()
{
    if (1 === preg_match('/[\d.-]+/', YAYUI_VERSION, $matches))
        $result['version'] = $matches[0];
    if (1 === preg_match('/[\d.-]+/', YAYUI_VERSION_DATE, $matches))
        $result['version_date'] = $matches[0];
    if (1 === preg_match('/@author:\s*([^$]*)/', YAYUI_AUTHOR, $matches)) {
        $author = $matches[1];
        if (1 === preg_match('/^\b[\w\s]+\b/', $author, $matches))
            $result['author_name'] = $matches[0];
        if (1 === preg_match('/<(.*)>/', $author, $matches))
            $result['author_email'] = $matches[1];
    }
    
    return $result;
}

if (! empty($_POST))
    $argv = $_POST;

if (! empty($argv) && array_key_exists('q', $argv) && array_key_exists('download_file', $argv))
    return sendFile(sys_get_temp_dir() . DIRECTORY_SEPARATOR . $argv['download_file']);
$options = parseArguments($argv, ! empty($_POST));
if (! (empty($options['help']) && empty($options['h'])))
    echoHelp();

$start = microtime();

if (array_key_exists('source_code', $argv))
    $buffer = $argv['source_code'];

if (count($argv) > 1 && array_key_exists(1, $argv))
    $in_file = $argv[1];
elseif (empty($buffer))
    echoHelp("Input filename not specified");

if (count($argv) > 2 && array_key_exists(2, $argv))
    $out_file = $argv[2];
elseif (! empty($buffer))
    $out_file = tempnam(sys_get_temp_dir(), 'yayui_');
else
    echoHelp("Output filename not specified");

$yayuic = new YayuiCompressor();
try {
    
    $fin = empty($buffer) ? filesize($in_file) : strlen($buffer);
    
    if (empty($buffer))
        $yayuic->compress($in_file, $out_file, $options);
    else {
        $buffer = $yayuic->streamCompress($buffer, $options);
        file_put_contents($out_file, $buffer);
    }
    $end = microtime() - $start;
    
    $fout = empty($buffer) ? filesize($out_file) : strlen($buffer);
    
    echoStatus('Compression done successfuly!');
    
    if (empty($buffer)) {
        echo "Source file : $in_file" . PHP_EOL;
        echo "Compressed file : $out_file" . PHP_EOL . PHP_EOL;
    }
    
    echo sprintf('%-16s : %10d bytes' . PHP_EOL, 'Original size', $fin);
    echo sprintf('%-16s : %10d bytes' . PHP_EOL, 'Compressed size', $fout);
    echo sprintf('%-16s : %10.2f %%' . PHP_EOL, 'Compress ratio', $yayuic->getRatio());
    echo sprintf('%-16s : %10.3f sec' . PHP_EOL, 'Elapsed time', $end > 0 ? $end : 0);
    
    $data = $yayuic->getStatistics();
    echo PHP_EOL;
    echoStatistic($data, $fin);
    
    if (! empty($buffer))
        echo "<!--download_file=" . basename($out_file) . "-->";
} catch (Exception $err) {
    echoStatus('Unexpected error: ' . $err->getMessage());
}

?>