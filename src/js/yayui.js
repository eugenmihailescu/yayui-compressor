/**
 * ----------------------------------------------------------------------------
 * Copyright 2014 Eugen Mihailescu (email : eugenmihailescux at gmail dot com)
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, version 2, as published by the
 * Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 * 
 * ----------------------------------------------------------------------------
 * 
 * Plugin Name: YAYUI Compressor (Yet Another YUI Compressor)
 * 
 * Plugin URI: http://yayui.mynixworld.info/
 * 
 * Description: YAYUI Web Interface client scripts
 * 
 * Author URI: http://yayui.mynixworld.info/
 * 
 * Git revision information:
 * 
 * @version:	0.1-2 $
 * @commit:		17c98134e99dc43433265cf28b9440b01f04828d $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Mon Oct 13 17:51:28 2014 +0200 $
 * @file: yayui.js $
 * 
 * @id:	yayui.js | Mon Oct 13 17:51:28 2014 +0200 | Eugen Mihailescu  $
 * 
 */

/* jslint browser:true */
var UNDEFINED = 'undefined';

function s4() {
    "use strict";
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
}

function toggle_children(parent) {
    "use strict";
    if (parent) {
        var children = document.getElementsByTagName('input'), i;
        for (i = 0; i < children.length; i += 1) {
            if (children[i].name.indexOf(parent.id + '_') >= 0) {
                children[i].checked = parent.checked;
            }
        }
    }
}

function toggle_parent(child) {
    "use strict";
    if (child) {
        var siblings = document.getElementsByTagName('input'), all_checked =
                true, prefix =
                child.name.substring(0, child.name.indexOf('_') + 1), parent =
                document.getElementById(prefix.substring(0, prefix.length - 1)), i;
        if (!parent) {
            return;
        }

        for (i = 0; i < siblings.length && all_checked; i += 1) {
            if (siblings[i].name.indexOf(prefix) >= 0) {
                all_checked = all_checked && siblings[i].checked;
            }
        }

        parent.checked = all_checked;
    }
}

function isNull(obj, value) {
    "use strict";
    return (typeof obj === UNDEFINED || obj === null) ? value : obj;
}

/**
 * Close the popup
 * 
 * @param popup
 */
function removePopup(popup) {
    "use strict";
    document.onkeydown = "";
    document.body.removeChild(popup);
    popup = null;
}

/**
 * Create and show the download popup window
 * 
 * @param title
 * @param content
 * @param width
 * @param height
 * @param color
 * @returns {___newdiv3}
 */
function popupWindow(title, content, width, height, titlecolor, reuse_div, autoscroll, confirmclose) {
    "use strict";
    titlecolor = isNull(titlecolor, '#2ca8d2');
    confirmclose = isNull(confirmclose, false);

    var uuid, i, h, w, name, divIdName, window_width, window_height, newdiv, div_exists, clientX, clientY, style, inner_div, divs =
            document.getElementsByTagName('div'), titlebar =
            document.getElementById('titlebar'), titlebarHeight, adminBar =
            document.getElementById('wpadminbar'), adminBarHeight, cmsg =
            "Are you sure you want to close this window?";
    if (titlebar) {
        titlebarHeight = titlebar.clientHeight;
    } else {
        titlebarHeight = 30;
    }

    if (adminBar) {
        adminBarHeight = adminBar.clientHeight;
    } else {
        adminBarHeight = 0;
    }

    div_exists =
            (reuse_div && document.getElementById(reuse_div.id) && document
                    .getElementById(reuse_div.id).style);
    if (!div_exists) {
        for (i = 0; i < divs.length; i += 1) {
            name = divs[i].getAttribute('id');
            if (name && name.indexOf('popupDiv_') === 0) {
                removePopup(divs[i]);
            }
        }
        if (!reuse_div) {
            // create an unique name div
            uuid = s4() + s4() + s4() + s4();
            newdiv = document.createElement('div');
            divIdName = 'popupDiv_' + uuid;
            newdiv.setAttribute('id', divIdName);
        } else {
            newdiv = reuse_div;
            divIdName = newdiv.id;
        }
        // set mouse drag events
        newdiv.onmousedown =
                function(event) {
                    clientY = event ? event.clientY : window.event.clientY;
                    clientX = event ? event.clientX : window.event.clientX;

                    titlebar = document.getElementById('titlebar');
                    if ((clientY) > Number(this.style.top.replace('px', ''))
                            + 5 /* padding */+ titlebar.clientHeight
                            || clientX > Number(this.style.left
                                    .replace('px', ''))
                                    + titlebar.clientWidth - 30) {

                        return;
                    }

                    window.offset_y = clientY - this.offsetTop;
                    window.offset_x = clientX - this.offsetLeft;
                    window.dragging = true;
                    this.style.cursor = "move";
                };
        newdiv.onmouseup = function() {
            this.style.cursor = "initial";
            window.dragging = false;
        };
        newdiv.onmousemove = function(event) {
            if (!window.dragging) {
                return;
            }
            clientY = event ? event.clientY : window.event.clientY;
            clientX = event ? event.clientX : window.event.clientX;
            this.style.top = (clientY - window.offset_y) + "px";
            this.style.left = (clientX - window.offset_x) + "px";
            this.style.cursor = "move";
        };
    } else {
        newdiv = reuse_div;
        divIdName = newdiv.id;
    }

    // add ESC listener event
    document.onkeydown =
            function(a) {
                a = a || window.event;
                if (a.keyCode === 27
                        && newdiv && (!confirmclose || window.confirm(cmsg))) {
                    removePopup(newdiv);
                    newdiv = null;
                }
            };

    // create the div HTML content
    newdiv.innerHTML =
            '<table id="titlebar" width="100%">'
                    + '<tr bgcolor="'
                    + titlecolor
                    + '"><td><table width="100%"><tr><td style="font-weight:bold;color:#fff;">'
                    + title
                    + '</td><td style="text-align:right"><a style="text-decoration:none;font-weight:bold;color:#fff;cursor:pointer" onclick="'
                    + (confirmclose ? 'if(confirm(\'' + cmsg + '\'))' : '')
                    + 'removePopup('
                    + divIdName
                    + ');">close</a> or Esc key</td></tr></table></td></tr></table>';
    newdiv.innerHTML +=
            '<div id="'
                    + divIdName
                    + '_inner" style="@INNER-STYLE@" onmousedown="return"><table><tr><td>'
                    + content + '</div>';

    // stylish the DIV
    newdiv.style.width = isNull(newdiv.style.width, width + "px");
    newdiv.style.height = isNull(newdiv.style.height, width + "px");

    newdiv.style.position = "fixed";
    newdiv.style.padding = "5px";
    newdiv.style.border = "2px solid";
    newdiv.style.borderColor = "#a1a1a1";
    newdiv.style.borderRadius = "10px";
    newdiv.style.boxShadow = "10px 10px 5px rgba(0,0,0,0.5)";
    newdiv.style.background = "white";
    newdiv.style.color = "black";
    newdiv.style.select = 'none';
    newdiv.style.zIndex = 1000;

    // inject the HTML content inside DIV
    if (!div_exists) {
        document.body.appendChild(newdiv);
    }

    style = 'width:100%;';
    if (newdiv.clientHeight > window.innerHeight - 100 - adminBarHeight) {
        h = window.innerHeight - adminBarHeight - 50;
        style +=
                'overflow:scroll;height:'
                        + (h - titlebarHeight - adminBarHeight) + 'px;';

        newdiv.style.top = "50px";
        newdiv.style.height = h + 'px';
    }
    if (newdiv.clientWidth > window.innerWidth - 100) {
        w = window.innerWidth - 50;
        style += 'overflow:auto;width:' + (w - 10) + 'px;';
        newdiv.style.left = "50px";
        newdiv.style.width = w + 'px';
    }
    if (style.length > 0) {
        newdiv.innerHTML = newdiv.innerHTML.replace('@INNER-STYLE@', style);
    }
    window_width =
            window.innerWidth
                    || document.documentElement.clientWidth
                    || document.body.clientWidth;
    window_height =
            window.innerHeight
                    || document.documentElement.clientHeight
                    || document.body.clientHeight;

    newdiv.style.left = (window_width - newdiv.clientWidth) / 2 + 'px';
    newdiv.style.top = (window_height - newdiv.clientHeight) / 2 + 'px';

    // scroll to bottom
    if (autoscroll === true) {
        inner_div = document.getElementById(divIdName + '_inner');
        if (inner_div) {
            inner_div.scrollTop = inner_div.scrollHeight;
        }
    }

    return newdiv;
}

/**
 * @param title
 * @param content
 * @param width
 * @param height
 */
function popupError(title, content, width, height) {
    "use strict";
    popupWindow(title, content, width, height, '#ff0000');
}

// Send a request (see method) to the server specified by url with the
// parameters specified by params. The request can be (a)synchronous and also
// can have a callback function were by default we send back a reference to the
// XMLHttpRequest object and the sent time.
function ajaxRequest(url, params, async, callback, method) {
    'use strict';
    // Note: this should be used within the same domain (don't violate CORS)
    // On Firefox we only get a warning on Developer Console tool

    if (!method) {
        method = 'POST'; // default
    }

    var xmlhttp, start = new Date();

    xmlhttp =
            window.XMLHttpRequest ? new window.XMLHttpRequest() : new window.ActiveXObject('Microsoft.XMLHTTP');

    if (callback) {
        /* jslint white: true */
        xmlhttp.onreadystatechange = function() {
            callback(xmlhttp, start);
        };
    }

    if (url) {
        try {
            xmlhttp.open(method, url, async);
            if (params && params.length > 0) {
                xmlhttp
                        .setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
                xmlhttp.setRequestHeader('Content-length', params.length);
                xmlhttp.setRequestHeader('Connection', 'close');
            }

            xmlhttp.send(params);
        } catch (err) {
            if (callback) {
                callback('JavaScript error: ' + err.message);
            }
        }
    } else {
        throw 'URL not specified!';
    }
    return xmlhttp;
}

function asyncRunJob(url, params, title, prefix, sufix, state, element, spin_id, onready) {
    "use strict";
    var div = null, hide = false, obj, el, callback;
    if (!url) {
        return;
    }

    prefix = isNull(prefix, '');
    sufix = isNull(sufix, '');
    title = isNull(title, '');
    state = isNull(state, 3);
    element = isNull(element, false);
    spin_id = isNull(spin_id, 0);

    callback =
            function(xmlhttp) {
                if (xmlhttp) {
                    if (xmlhttp.substring) {
                        window.alert(xmlhttp);
                        hide = true;
                    } else if (xmlhttp.readyState >= state) {
                        // http://msdn.microsoft.com/en-us/library/ms753800%28v=vs.85%29.aspx
                        if (typeof (xmlhttp.responseText) !== "unknown") {
                            if (!element) {
                                div =
                                        popupWindow(title + ' console', prefix
                                                + xmlhttp.responseText + sufix, null, null, null, div, true);
                            } else {
                                obj = document.getElementById(element);
                                if (obj) {
                                    obj.innerHTML = xmlhttp.responseText;
                                }
                            }
                        }
                        hide = xmlhttp.readyState === 4;
                    }
                    if (hide) {
                        el = document.getElementById(element);
                        if (el) {
                            el.className = el.className.replace('loading', '');
                        }
                        if (onready) {
                            onready(xmlhttp);
                        }
                    }
                }
            };
    ajaxRequest(url, params, true, callback);
}

function asyncGetContent(url, params, element, onready) {
    "use strict";
    asyncRunJob(url, params, null, null, null, 4, element, 0, onready);
}

function info() {
    "use strict";
    var style, exec_btn, info_btn, info_table, main_table;
    exec_btn = document.getElementById('exec_btn');
    info_btn = document.getElementById('info_btn');
    info_table = document.getElementById('info_table');
    main_table = document.getElementById('main_table');
    // output_table.setAttribute('style', 'display: none');
    if (info_table.style.display === 'block') {
        style = 'none';
        info_btn.innerHTML = 'More info &gt;&gt;';
        info_btn.setAttribute('class', 'button-primary');
        main_table.setAttribute('style', 'display: block');
        exec_btn.setAttribute('style', 'display: block');
    } else {
        info_btn.innerHTML = '&lt;&lt; Hide info';
        info_btn.setAttribute('class', 'button fbold');
        style = 'block';
        main_table.setAttribute('style', 'display: none');
        exec_btn.setAttribute('style', 'display: none');
    }
    info_table.setAttribute('style', 'display:' + style);

}

function getGaActionBySrcLen(srclen) {
    if (srclen < 1000)
        return 1;
    if (srclen < 10000)
        return 10;
    if (srclen < 30000)
        return 30;
    return '30+';
}

function compress() {
    "use strict";
    var url = 'yayui-cli.php', groups = ['minify', 'obfuscate'], i, j, children =
            document.getElementsByTagName('input'), source_code =
            document.getElementById('source_code').value, params = '', onready, p, prefix, key, durl, output_table, main_table, info_table, callback;

    if (source_code.length === 0) {
        _gaq.push(['_trackEvent', 'yayui-compress-0', 'error']);
        return popupError('Opps', 'Your source code seems to be empty.<br>Are you just having fun?');
    }
    if (source_code.length > 5242880 /* 5MB */) {
        _gaq.push(['_trackEvent', 'yayui-compress-5M+', 'error']);
        return popupError('Opps', 'It seems that you are trying to compress a serious stuff, isn\'t it?<br><br>I\'m trully sorry. Although I might help you my boss set a <b>5MB</b><br>source code <b>limit</b> :-(<br><br>Please try and send me chuncks of code, would you?');
    }

    info_table = document.getElementById('info_table');
    info_table.setAttribute('style', 'display: none');
    main_table = document.getElementById('main_table');

    // show loading bar
    main_table.setAttribute('class', 'centered loading');

    for (i = 0; i < groups.length; i += 1) {
        for (j = 0; j < children.length; j += 1) {
            if (children[j].name.indexOf(groups[i] + '_') >= 0
                    && children[j].checked) {
                params +=
                        '-' + children[j].id + '=' + children[j].checked + '&';
            }
        }
    }
    params +=
            'source_code='
                    + encodeURIComponent(document.getElementById('source_code').value);

    // when receiving the compression status => download the compressed source
    onready =
            function(xmlhttp) {

                // when receiving the compressed source
                callback = function(http) {
                    main_table.setAttribute('class', 'centered');
                    output_table = document.getElementById('output_table');
                    output_table.setAttribute('style', 'margin-bottom:20px');
                };

                key = 'download_file';
                prefix = '<!--' + key + '=';
                p = xmlhttp.responseText.indexOf(prefix);
                if (p >= 0) {
                    durl =
                            xmlhttp.responseText
                                    .substring(p + prefix.length, xmlhttp.responseText.length - 3);

                    // download the compressed source and let the callback to
                    // print it out
                    asyncGetContent(url, 'q=download&'
                            + key + '=' + encodeURIComponent(durl), 'output', callback);
                }
            };
    // request source code compress
    asyncRunJob(url, params, 'Result', "<pre style='color:#0f0;background-color:#000'>", '</pre>', 3, null, null, onready);

    _gaq.push(['_trackEvent', 'yayui-compress-'
            + getGaActionBySrcLen(source_code.length), 'compress']);

}

function toggle_feedbackbox() {
    var feedback_box = document.getElementById('feedback_box');
    if (feedback_box.style.left === 0 || feedback_box.style.left === '0px') {
        feedback_box.setAttribute('style', 'left:-'
                + feedback_box.offsetWidth + 'px');
    } else {
        feedback_box.setAttribute('style', 'left:0px');
    }
}