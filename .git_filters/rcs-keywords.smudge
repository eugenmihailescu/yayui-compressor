#!/usr/bin/perl
#
# @brief  Git filter to implement rcs keyword expansion as seen in cvs and svn.
# @author Martin Turon
#
# Usage:
#    .git_filter/rcs-keywords.smudge file_path < file_contents
# 
# To add keyword expansion:
#    <project>/.gitattributes                    - *.c filter=rcs-keywords
#    <project>/.git_filters/rcs-keywords.smudge  - copy this file to project
#    <project>/.git_filters/rcs-keywords.clean   - copy companion to project
#    ~/.gitconfig                                - add [filter] lines below
#
# [filter "rcs-keywords"]
#	clean  = .git_filters/rcs-keywords.clean
#	smudge = .git_filters/rcs-keywords.smudge %f
#
# Copyright (c) 2009-2011 Turon Technologies, Inc.  All rights reserved.

$path = shift;
$path =~ /.*\/(.*)/;
$filename = $1;

if (0 == length($filename)) {
	$filename = $path;
}

# Need to grab filename and to use git log for this to be accurate.
$rev = `git log -- $path | head -n 3`;
$rev =~ /^Date:\s*(.*)\s*$/m;
$rev =~ /^Author:\s*(.*)\s*$/m;
$author = $1;
$author =~ /\s*(.*)\s*<.*/;
$name = $1;
$rev =~ /([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})/;
$email=$1;
$rev =~ /^Date:\s*(.*)\s*$/m;
$date = $1;
$rev =~ /^commit (.*)$/m;
$commit = $1;
$short_ver = `git describe --long|egrep -o '^([0-9.-]*)[0-9]'`;
$short_ver =~ /^(.*)/m;
$short_ver = $1;
$tag = `git describe`;
$short_date = `git log -1 --date=short`;
$short_date =~ /^Date:\s*(20\d\d)-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])\s*/m;
$short_date = "$1-$2-$3";

while (<STDIN>) {
    s/\@file\s+(.*)*\$/\@file:\t\t$filename \$/;
    s/\@source\s*(.*)*\$/\@source:\t\t$path \$/;
    s/\@short_date\s*(.*)*\$/\@short_date:\t$short_date \$/; 
    s/\@date\s*(.*)*\$/\@date:\t\t$date \$/;
    s/\@author\s*(.*)*\$/\@author:\t\t$author \$/;
    s/\@id\s*(.*)*\$/\@id:\t$filename | $date | $name \$/;
    s/\@version\s*(.*)*\$/\@version:\t$short_ver \$/;
    s/\@commit\s*(.*)*\$/\@commit:\t\t$commit \$/;
    s/\@email\s*(.*)*\$/\@email:\t\t$email \$/;
    s/\@tag\s*(.*)*\$/\@tag:\t$tag \$s/;
    s/\d+.\d+-rc?-\d+-\w{1,8}/$short_ver/;
} continue {
    print or die "-p destination: $!\n";
}

